### Learn PostgreSQL by practice!

This guide ~~is written so my dumbass remembers what's going on~~ should help you get started with PostgreSQL (PG from now) on Ubuntu by importing a sample DB with mock data to play around on your local pc.

It is a combination / consolidation of a few materials as listed below:

1. [DO - Setting up PG on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04)
2. [PostgreSQL Tutorial - Import DB with mock data and practice](http://www.postgresqltutorial.com/)
3. [PostgreSQL Tutorial - Handy commands](http://www.postgresqltutorial.com/psql-commands/)

**Remarks: Original articles are more detailed with explanations. (duh)**



---

## Installing PG on Ubuntu

1. `sudo apt update` - Updates packages

2. `sudo apt install postgresql postgresql-contrib` - Installs PG 10 packages at the time of writing

    > There should be a default user for PG, `postgres` being created.

3. `sudo -i -u postgres` - Switches from your current user from `user@pcname` to `postgres@pcname` (Almost like "Logging in to PG with a different user.")

4. `psql` - Opens up PG command prompt

5. `select version();` - Should echo out PG version without problem.

6. `\q` - Exits PG prompt

7. `exit` - Goes back to `user@pcname`.



---

## Adding a new user (Optional)

`sudo adduser <username>` - Key in passwords and you're done.



---

## Importing DB - prepping files

1. Download (and unzip) the sample zip file from [PG tutorial page](http://www.postgresqltutorial.com/postgresql-sample-database/) into your project folder.

2. In `restore.sql`, replace `$$PATH$$/*.dat` with `/absolute/path/to/project/*.dat` files.

3. Make sure `toc.dat` file is in place.

4. Optional: At the end of `restore.sql`, you should see 4 lines `revoke all ...` x2 and `grant all ...` x2, I have replaced `postgres` with the **specific user** I've created for this practice.*

*What this does? [Read more](https://dba.stackexchange.com/questions/35316/why-is-a-new-user-allowed-to-create-a-table#answers-header)



---

## There are **two ways** to import DB:

## Via user session

1. `sudo -i -u <username>` - Log in as the PG user

2. `createdb dvdrental -e` - Create a new DB "dvdrental"
    > `-e` flag will echo out the "actual sql statement" being created and executed.
    >
    > `man createdb` to learn more.

3. `psql dvdrental < /absolute/path/to/restore.sql`
    > If you hit any permission errors, you can try pulling up a **separate terminal session**, and run `chmod a+r /path/to/file-a.ext /path/to/file-b.ext ...` to each of the files that hit this error.

4. `psql -l` - List out all available DBs without opening PG prompt
    > You should see the new DB being listed along with other default DBs.



---

## Via PG prompt (content below WIP)

1. `sudo -u <username> psql -d dvdrental` - Log in as "\<username\>" and connect to db "dvdrental"
    > `\conninfo` to see more about this log in.

2. `\dt` - Should list out all the available
